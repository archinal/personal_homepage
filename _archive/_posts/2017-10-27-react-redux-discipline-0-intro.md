---
layout: post
title:  "React/Redux Discipline: Part 0 - Introduction"
description: "Introducing a new series of blog posts - lessons and opinions on best practices"
img: "React_Redux_Discipline_Part_0.png"
date: 2017-10-27  +1100
categories: [javascript, react, redux]
---

I've been working on React apps for a few years now. 
I've used it across multiple projects, in things like monitoring dashboards, serverless IoT platforms, and data-heavy SaaS platforms. 
I originally used [Flux](https://facebook.github.io/flux/docs/overview.html) as a state management system, but have since moved on to [Redux](https://github.com/reactjs/redux).

Although I'm now a huge advocate of Redux, my enjoyment of it has had to develop over time. 
Generally speaking, the more disciplined I've been about the way I stored, accessed, and updated the Redux store (that is, the application state),
the happier I've become.

Anyone who has spent a significant amount of time working on a React-based single page web application has seen the potential for things to break out into incomprehensible spaghetti code.
Small lapses in best practice here and there quickly snowball into a monstrosity that almost can't be redeemed.
Fortunately, if you adhere to a consistent, strict way of handling your application state, the application conversely becomes extremely cohesive, predictable, readable, and easy to test.  

I've created this series of blog posts to document the lessons I've learned, and to communicate what has worked best for me over time. 

***

## Posts in this series
{% include series/toc-react-redux-discipline.md %}