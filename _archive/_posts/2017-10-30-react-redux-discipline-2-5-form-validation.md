---
layout: post
title:  "React/Redux Discipline: Part 2.5 - Real-time form validation with redux-form and Joi schemas"
description: "Re-use your schemas to perform real-time form validation and tips for your users."
img: "React_Redux_Discipline_Part_2-5.png"
date: 2017-10-30  +1100
categories: [javascript, react, redux]
---

In our [previous post]({% post_url 2017-10-29-react-redux-discipline-2-enforcing-data-validity %}) we covered how to use [Joi](https://github.com/hapijs/joi) schemas to validate the data in our application.
In this post we will go through how to use these schemas to create low-effort-high-value form validators for [`redux-form`](https://redux-form.com).

## Brief introduction to redux-form
[`redux-form`](https://redux-form.com) is a library that helps us manage the state of our application's forms in Redux.
The general idea is that the state of forms (their current value, if they've been "touched", if there are validation errors, ...) is stored in the Redux store. 
This means our components can read and update the values in these forms in a clean way, consistent with the disciplined patterns we've previously established.
On top of this, `redux-form` gives us rich functionality like:
- Dynamic form creation
- Real time or asynchronous form validation
- Support for custom form fields as first-class citizens
- Support for "sub-forms"
- A simple way to provide initial values (which is great for editing our `User` objects)

This post will focus on form validation. 

## Validating the data
I won't go into detail about how to use `redux-form` here - I'll refer you to its [(excellent) documentation](https://redux-form.com/7.1.2/docs/gettingstarted.md/) for that.
For this example we'll be working on an imaginary `UserCreateEditForm` component, which creates `User` objects consistent with what we spoke about in the [previous post]({% post_url 2017-10-29-react-redux-discipline-2-enforcing-data-validity %}).

This form will have `Field`s with the following names:
- `username`
- `email`
- `phone`
- `name`

To make `redux-form` validate the form data against our schema in real-time, we simply need to do the following:

First, write a utility function `createJoiFormValidator`:
{% highlight jsx %}
import Joi from 'joi-browser';

const createJoiFormValidator = schema => {
    return values => {
        const errors = {};
        const result = Joi.validate(values, schema, {
            abortEarly: false,
        });
        if (result.error) {
            result.error.details.forEach(detail => {
                const fieldName = detail.path;
                // Perform some magic to make the error messages more human-readable
                errors[fieldName] = detail.message.replace(
                    `"${fieldName.slice().split('.').pop()}"`,
                    'This'
                );
            });
        }
        return errors;
    };
};

export default createJoiFormValidator;
{% endhighlight %}

Next, add the following line to the part of the `UserCreateEditForm` where you call the `reduxForm` function:
{% highlight jsx %}
import userSchema from './schemas/user';

// ... Define UserCreateEditForm component here

const RFUserCreateEditForm = reduxForm({
    form: 'UserCreateEditForm',
    validate: createJoiFormValidator(userSchema),
})(UserCreateEditForm);

export default RFUserCreateEditForm;
{% endhighlight %}

That's it! This validator approach has the following benefits:
- It gives a human-readable error message if something is wrong (like "This is required", or "This must not be more than 50 characters")
- It doesn't render an error if the user hasn't touched that field yet (unless they try to submit the form).
This prevents that annoying case where a form yells "This is required" at you before you even get to that part.

The [next, and final]({% post_url 2017-10-31-react-redux-discipline-3-layout-and-naming %}) post in this series will cover some naming and code layout patters that have worked well for me over time. 

***

## Posts in this series
{% include series/toc-react-redux-discipline.md %}