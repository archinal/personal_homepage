---
layout: post
title:  "Using Prettier to keep consistent code style"
description: "End the great code style debates, keep consistent code, and save ESLint for the important stuff."
img: "prettier.png"
date: 2017-10-26  +1100
categories: [javascript]
---

## The Problem
If you've ever collaborated on a codebase you'll know that debating style is always a losing battle.
Even if you can get everyone to agree on how things should be styled, you'll still run into the following problems:
- People will make mistakes, and if someone else goes in to fix those mistakes they'll own the line of code in git history
- People will inconsistently enforce the rules
- You'll frustrate new team members by rejecting/delaying their pull requests for style issues
- Team members who get sick of the rules might remove the style linter from the build process, and things like ESLint can be crucial
for picking up potential errors in code.

These issues are twice as problematic in a language like JavaScript, where there are so many things to debate
(don't believe me? [ESLint has 89 rules for "stylistic issues"](https://eslint.org/docs/rules/#stylistic-issues),
and that's if you don't count the additional rules for ECMAScript 6 or Node.js).

The title of this blog post makes it clear what alternative I'm about to suggest, but before I do I'd just like to make
a distinction between two aspects of code linting:

On one hand we have __stylistic code linting__. This is to enforce consistent code style, enforcing rules like:
- [Consistent brace style](https://eslint.org/docs/rules/brace-style)
- [Consistent spacing before and after commas](https://eslint.org/docs/rules/comma-spacing)
- [Consistent indentation](https://eslint.org/docs/rules/indent)

On the other hand, we have __functional code linting__, which exists to unearth possible errors like:
- [Preventing duplicate keys in objects](https://eslint.org/docs/rules/no-dupe-keys)
- [Preventing duplicate arguments in functions](https://eslint.org/docs/rules/no-dupe-args)
- [Preventing comparisons of `typeof` to strings that don't represent types](https://eslint.org/docs/rules/valid-typeof)

To be clear, I believe the solution I'm about to suggest should replace stylistic code linting, not functional linting
(you should be using a tool like [ESLint](https://eslint.org/) for the latter).

## The Solution
Enter [Prettier](https://prettier.io/). Prettier is a code formatter that works not only with JavaScript, but also with CSS, SCSS, LESS, JSON, JSX, TypeScript... you get the idea.
When you run Prettier on your codebase it will strip down the code and completely reformat it according to its own (customizable) style guide.

This means developers are able to write code with their own personal style, run it through Prettier, and have it conform 100% to the style guide.

### How to adopt it
Prettier is at its most powerful when you integrate it as a git pre-commit hook, as it means the code you commit to the repository never deviates from the style guide.

To make this happen in a standard npm project, you'll first need to install [`husky`](https://www.npmjs.com/package/husky) (which lets you run commit hooks like they're npm scripts)
 and [`lint-staged`](https://www.npmjs.com/package/lint-staged) (which lets you run scripts on staged files in git) alongside `prettier`:

{% highlight bash %}
npm install --save-dev husky lint-staged prettier
{% endhighlight %}

Next, add the following line to your `package.json` "scripts":

```diff
  "scripts": {
+   "precommit": "lint-staged",
  }
```

Next we add a "lint-staged" field to `package.json`:

```diff
+ "lint-staged": {
+   "src/**/*.{js,jsx,json,scss}": [
+     "prettier --single-quote --tab-width 4 --trailing-comma es5 --write",
+     "git add"
+   ]
+ },
```

You can change the third line on the script above to configure the style guide prettier employs.
The options written above are what I use personally.
The "regex" on the second line can also be configured to affect the kind of files you want to target.

If you're integrating prettier with an existing project, you might like to run it manually on all your files:

{% highlight bash %}
node node_modules/prettier/bin/prettier.js --single-quote --tab-width 4 --trailing-comma es5 --write ./**/*.js
{% endhighlight %}

That's it! Now, as I mentioned before, you should still be using a code linter to check for potential errors,
but Prettier will put an end to the squabbles over stylistic issues once and for all.