---
layout: post
title:  "React/Redux Discipline: Part 3 - Thoughts on naming conventions and app layout"
description: "Thoughts on naming and layout conventions that have served me well over time."
img: "React_Redux_Discipline_Part_3.png"
date: 2017-10-31  +1100
categories: [javascript, react, redux]
---

For the final installation in this series, I will cover a few variable naming and code layout conventions that have worked well for me over time.

> Note that these suggestions are very much a matter of personal preference. 
The point of this post is in part just to get you to consider the naming and layout decisions you're making in your projects.
Overall, the high-level recommendation I'm trying to make is __"be structured and consistent"__. 

## Code Layout
For my recommendations here, let's consider a dummy layout of the User List app we've been making in this series, 
supposing we have the concept of "Users" and "Teams":
```
./src/
├── api
│   ├── team.js
│   └── user.js
├── components
│   ├── team
│   │   ├── TeamCreateEditForm.js
│   │   ├── TeamList.js
│   │   ├── TeamListItem.js
│   │   └── TeamPage.js
│   ├── user
│   │   ├── UserCreateEditForm.js
│   │   ├── UserList.js
│   │   └── UserListItem.js
│   └── routes
│       ├── AppRouter.js
│       └── urls
│           └── index.js
├── index.js
├── models
│   ├── schemas
│   │   ├── user.js
│   │   └── team.js
│   ├── user.js
│   └── team.js
├── redux
│   ├── actions
│   │   ├── index.js
│   │   ├── team.js
│   │   └── user.js
│   ├── forms
│   │   ├── index.js
│   │   └── team.js
│   ├── reducers
│   │   ├── index.js
│   │   ├── team.js
│   │   └── user.js
│   ├── sagas
│   │   ├── index.js
│   │   ├── team.js
│   │   └── user.js
│   └── selectors
│       ├── index.js
│       ├── team.js
│       └── user.js
└── utils
    └── createJoiFormValidator.js
```

- At a high level, the app is split into:
  - API stuff (`api/`)
  - React components (`components/`), sorted by relevant model
  - Model stuff (`models/`)
  - Redux stuff (`redux/`)
  - Other utilities (`utils/`)
- Within the `redux/` directory, I split each model concept of the app (`user`, `team`) into its actions, reducer, sagas, and selectors.
These are then each categorised into their own distinct files. This makes finding the sagas/reducer/actions/selectors relevant to a certain data model simple.
This is a similar, but not exactly equal, concept to the idea of ["ducks"](https://github.com/erikras/ducks-modular-redux), put forward by erikras, creator of `redux-form`.

## Naming Conventions
### Components
- The name of the components should be of the format `${CONTEXT}${PURPOSE}`, like `UserList.js` or `TeamCreateEditForm.js`
  - Placing the `CONTEXT` at the front of the name helps group components logically when they're listed alphabetically

### Actions
- Action names should be prefixed with the name of your application, such as `my-app/USER_LIST_GET_REQUESTED`. 
This will namespace the actions, (probably) preventing them from being mixed up with actions dispatched by third party packages like `redux-form`.
This in turn makes your Redux action history more readable. 
- As mentioned in [Part 1]({% post_url 2017-10-28-react-redux-discipline-1-state-management %}) of the series, side-effectful actions should be split into:
  - `*_REQUESTED` to kick off the async/impure request
  - `*_SUCCEEDED` if it succeeds
  - `*_FAILED` if it fails
- Action bodies should be [Flux Standard Actions](https://github.com/acdlite/flux-standard-action)

### General
- When using a model name in a file name (e.g. `api/team.js`), always use it in the singular form of the word (so not `teams.js`).

## Series Conclusion
I've made a lot of recommendations in this series, because I've learned a lot over time. 
If you only take away a small piece of advice from this, however, make it the following:
- Be consistent
- Be structured
- Be predictable
- Remember that you might need to look at your code after a 12 month break

***

## Posts in this series
{% include series/toc-react-redux-discipline.md %}