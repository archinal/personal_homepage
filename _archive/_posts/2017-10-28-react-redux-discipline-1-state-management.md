---
layout: post
title:  "React/Redux Discipline: Part 1 - State storage and retrieval patterns"
description: "Best practices and recommendations around reading and updating the application state."
img: "React_Redux_Discipline_Part_1.png"
date: 2017-10-28  +1100
categories: [javascript, react, redux]
---

## Introduction

In the [introduction to this series]({% post_url 2017-10-27-react-redux-discipline-0-intro %}) I outlined the importance of being disciplined about the way we access, store, and update our applcation state in React/Redux applications.
In this post I will introduce a data-flow to explain how we can do all these things in a consistent, maintainable way.
I will also introduce a few third party packages that help us out here.

For the examples in this series we will be discussing an imaginary "user management" app. This app:
- Fetches a list of users from some API
- Presents the users in the UI as a list
- Lets us select a user to see more information about them
- Lets us create new users
- Lets us edit existing users

Our code examples in this series are written using [ECMAScript 6](http://es6-features.org/), and [JSX](https://reactjs.org/docs/introducing-jsx.html).
For this code to reliably run in modern browsers, it will need to pass through Webpack and some Babel transform. 
Facebook's [`create-react-app`](https://github.com/facebookincubator/create-react-app) will do all of this for you.

## The Data Flow
The following image depicts the pattern I've developed over time to handle state in React/Redux apps:

![React/Redux data flow]({{site.baseurl}}/assets/img/react_redux_data_flow.png)

There's a bit going on here, so I'm going to go through the data flow by explaining how we read and update the state.

## Reading state
> tl;dr: Use [`reselect`](https://github.com/reactjs/reselect) to create selectors, never access the state directly (only use selectors)

Let's begin with establishing some patterns to read from the application state.
For our user app, we will have a `UserList` component which will need to get the list of users from our Redux store.
Suppose our Redux store's state tree ("state") looks like this:

{% highlight jsx %}
// The default, empty state
const state = {
    users: null,
    selectedUser: null,
};

// OR

// The state with some users - "users" is an object whose keys are the usernames, 
// and the values contain user data
const state = {
    users: {
        alice: {
            username: 'alice',
            email: 'alice@example.com',
            phone: '1000000',
            name: 'Alice Apple',
        },
        bob: {
            username: 'bob',
            email: 'bob@example.com',
            phone: '2000000',
            name: 'Bob Banana',
        },
    },
    selectedUser: 'alice', // The username of the selected user, or null
};
{% endhighlight %} 

We then have a `UserList` component which needs to access the data from the state tree to populate.
The best way to do this is to set up the `users` object from the state as some of the `props` of the `UserList` component.
The package [`react-redux`](https://www.npmjs.com/package/react-redux) is built for this. 
It gives us a function `connect`, which takes two functions as arguments, `mapStateToProps` and `mapDispatchToProps` (we won't use the latter in this example).
This function then produces another function that we can wrap our React component in. 
The result is that `mapStateToProps` will be able to fill the `props` of the `UserList` component with the values from the store's state tree.
The code for that looks like this:

{% highlight jsx %}
import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import UserListItem from './UserListItem';
import LoadingSpinner from './LoadingSpinner';

const mapStateToProps = (state) => {
    // TODO: Fill this out using selectors
    return {
        hasLoadedUsers: false,
        usersExist: false,
        users: {},
        selectedUser: null,
    };
};

class UserList extends Component {
    componentDidMount() {
        // Later in this post we will do some data fetching here
    }

    render() {
        if (!this.props.hasLoadedUsers) {
            return <LoadingSpinner/>;
        } else if (!this.props.usersExist) {
            return (
                <p>No users exist yet.</p>
            );
        } else {
            return (
                <ul>
                    {_.map(this.props.users, user => (
                        <UserListItem user={user} 
                                      isSelected={user.username === this.props.selectedUser}
                                      key={`user-list-item-${user.username}`}
                        />
                    )}
                </ul>
            );
        }
    }
}

const ConnectedUserList = connect(mapStateToProps)(UserList);

export default ConnectedUserList;

{% endhighlight %} 

> Note that for the sake of brevity we won't be implementing things like `propTypes` on the component, despite this being best practice.

Now for the good part - accessing the application state. 
We can see in our `mapStateToProps` function that we're passed the whole state as a function parameter, so it's tempting to just do the following:

{% highlight jsx %}
// Inefficient and hard to maintain - don't do this
const mapStateToProps = (state) => {
    return {
        hasLoadedUsers: state.users !== null,
        usersExist: Object.keys(state.users).length > 0,
        users: state.users,
        selectedUser: state.selectedUser,
    };
};
{% endhighlight %} 

There are two big problems with this approach:
1. If (__when__) we later change the way we store data in our redux store (e.g. changing key names, or changing `users` from a map to an array),
it can be hard to remember and refactor all the components that directly access the store. 
Furthermore, if we later introduce some new value in the store like `getUsersIsLoading`, we might accidentally become inconsistent in the way we compute `hasLoadedUsers`.
2. The properties for the `UserList` component will be recomputed every time the state tree changes. 
As the application becomes larger and we track more things in the state, this recomputation will become very expensive.

To solve these problems we will use [`reselect`](https://github.com/reactjs/reselect) to create "selectors".
These selectors are "memoized", which means they only recompute if the return values if their function inputs change.
This means that if we change `selectedUser` in the store, things like `usersExist`, which only cares about the value of `users` won't need to recompute.
Furthermore, if we make a rule that __you can only access the redux store via selector functions__ this will make the whole application much more maintainable and predictable, not to mention potentially faster.

With `reselect`, we create the following selectors:
{% highlight jsx %}
import { createSelector } from 'reselect';

const getUsers = state => state.users;
const getSelectedUser = state => state.selectedUser;

const hasLoadedUsers = createSelector(getUsers, (users) => users !== null);
const usersExist = createSelector(getUsers, Object.keys(state.users).length > 0);

const selectors = {
    getUsers,
    getSelectedUser,
    hasLoadedUsers,
    usersExist,
};

export default selectors;

{% endhighlight %} 

We now change the `mapStateToProps` function to use the new selectors:
{% highlight jsx %}
// Add this line to your imports
import selectors from './selectors';

// Inefficient and hard to maintain - don't do this
const mapStateToProps = (state) => {
    return {
        hasLoadedUsers: selectors.hasLoadedUsers(state),
        usersExist: selectors.usersExist(state),
        users: selectors.getUsers(state),
        selectedUser: selectors.getSelectedUser(state),
    };
};
{% endhighlight %} 

Now we:
- Have a consistent pattern for accessing state
- Can refactor the layout of the store without needing to update all our components (only the selectors)
- Don't need to recompute _all_ of our component props when something like `selectedUser` changes

Let's move on to how we update the application state.

## Updating state
We're going to split this section into two subsections, as per the data flow diagram. 
First we'll talk about "side-effect free" functions, using a `selectUser` function as an example.
Next we'll cover "side-effectful" functions like getting data from an API, using a `fetchUsers function as an example`.

### Side-Effect Free Updates
> tl;dr: Use `mapDispatchToProps` from `react-redux`'s `connect` method to dispatch actions and have them handled by the store.
When we talk about "side-effect free" updates, we're referring to actions with the following qualities:
- They're synchronous (which rules out fetching data from an API)
- They're pure functions on the state (so they don't affect things like the browser cache)

An example of a side-effect free update is selecting a user from the `UserList`, by clicking a `UserListItem`.

For these cases, we simply use the `mapDispatchToProps` function passed to `react-redux`'s `connect` function.
This will dispatch an action, which will be processed by the redux reducer and update the state accordingly.

The best way to show how this works is through an example. Let's start with the `UserListItem` component, which we hinted at in the example above.

{% highlight jsx %}

import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        selectUser: () => {
            dispatch({
                type: 'my-app/SET_SELECTED_USER',
                payload: {
                    username: ownProps.user.username,
                },
            })
        },
    };
};

class UserListItem extends Component {
    render() {
        return (
            <li onClick={this.props.selectUser} 
                style={this.props.isSelected ? {fontWeight: 'bold'} : {}}>
                {this.props.user.name}
            </li>
        );
    }
}

const ConnectedUserListItem = connect(null, mapDispatchToProps)(UserListItem);

export default ConnectedUserListItem;

{% endhighlight %}

> In some applications it may be prudent to write a library of "action creators" instead of writing one by hand like in the `selectUser` function above.
Whether or not you choose to do this is a judgement call that will depend on how many places call the same action.
I recommend that you either __always__ use action creators or __never__ use them - for the sake of consistency don't mix and match.

The main point about is the `selectUser` method in `mapDispatchToProps`. 
When processed by the reducer we're about to write, its effect on the application state will always be the same - it will set the value of `selectedUser`.

Let's write that reducer (I assume you've already set up redux in your app, so we'll just write the reducer function for reference):

{% highlight jsx %}
const initialState = {
    users: null,
    selectedUser: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        // This part handles the action we dispatched in selectUser
        case 'my-app/SET_SELECTED_USER':
            return Object.assign({}, state, {
                selectedUser: action.payload.username,
            });

        default:
            return state;
    }
};

export default reducer;

{% endhighlight %}

This is an example of a standard dispatch pattern in Redux. 
Things get more complicated when we need to handle side effects. 

### Side-Effectful Updates
> tl;dr: Use `mapDispatchToProps` to dispatch `*_REQUESTED` actions, 
and a side-effect handler like [`redux-saga`](https://github.com/redux-saga/redux-saga) to listen for these "requests", process them, and dispatch the relevant `*_SUCCEEDED` or `*_FAILED` actions.

As an example of a side-effectful update we'll re-visit the `UserList` component from above and fill out its `componentDidMount` method, where we'll request the list of users from the API.
For these API calls we don't know ahead of time if the request will succeed (as intended) or fail (if the API server is down, or the user loses their network connection).
Because of that, we establish this convention:

1. The component wants some data from the API, so it dispatches a `*_REQUESTED` action, like `USER_LIST_GET_REQUESTED`
1. The side-effect handler (we'll use [`redux-saga`](https://github.com/redux-saga/redux-saga), but [`redux-thunk`](https://github.com/gaearon/redux-thunk) is popular too) listens for that request, and addresses it by making the API request.
1. If the API request is successful, the side-effect handler dispatches a `*_SUCCEEDED` action, like `USER_LIST_GET_SUCCEEDED`, with the new data
1. If the API request is unsuccessful, the side-effect handler dispatches a `*_FAILED` action, like `USER_LIST_GET_FAILED`, which tells the store that the GET request failed.

The following is an example of this pattern. To begin, we dispatch the `*_REQUESTED` action in the `UserList` component:

{% highlight jsx %}
// ...

const mapStateToProps = (state) => {
    //...
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserList: () => {
            dispatch({
                type: 'my-app/USER_LIST_GET_REQUESTED',
            });
        }
    };
};

class UserList extends Component {
    componentDidMount() {
        this.props.getUserList();
    }

    render() {
        ...
    }
}

const ConnectedUserList = connect(mapStateToProps)(UserList);

export default ConnectedUserList;

{% endhighlight %}

Now we write a saga to listen for the action `my-app/USER_LIST_GET_REQUESTED` (for more information on sagas, check out the [`redux-saga` docs](https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html)):

{% highlight jsx %}

import { call, put, takeEvery } from 'redux-saga/effects';
import API from './api';

function* getAllUsers(action) {
    try {
        // Suppose API.getAllUsers is some function that returns a Promise
        const users = yield call(API.getAllUsers);
        yield put({
            type: 'my-app/USER_LIST_GET_SUCCEEDED',
            payload: {
                users,
            }
        });
    } catch (e) {
        yield put({
            type: 'my-app/USER_LIST_GET_FAILED',
            payload: {
                message: 'Get all users failed',            
            },
        });
    }
}

function* getAllUsersSaga() {
    yield takeEvery('my-app/USER_LIST_GET_REQUESTED', getAllUsers);
}

const sagas = [
    getAllUsersSaga,
];

export default sagas;

{% endhighlight %}

Finally we adjust the reducer to handle the new actions:

{% highlight jsx %}
const initialState = {
    users: null,
    selectedUser: null,
    getAllUsersError: null, // We now track the GET error too
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        //...
            
        // Note that 'my-app/USER_LIST_GET_REQUESTED' is missing, as that action doesn't affect our store
            
        case 'my-app/USER_LIST_GET_SUCCEEDED':
            return Object.assign({}, state, {
                users: action.payload.users,
            });
            
        case 'my-app/USER_LIST_GET_FAILED':
            return Object.assign({}, state, {
                getAllUsersError: action.payload.message,
            });

        //...
    }
};

export default reducer;

{% endhighlight %}

And that's it! 
When we dispatch the `*_REQUEST` action for getting users, our `UserList` component doesn't need to worry about how to handle success, failure, and Promises.

If we consistently adhere to this pattern it makes our lives easier in the following ways:
- Asynchronous API calls, and their success and failure, are all handled in sagas separated from the UI components
- Side-effectful actions have consistent names, so are easy to read and reason about in the Redux action history
- The only place our API methods are called is the sagas module, which makes refactoring and testing (via mocking) easier.

## Conclusion
This post has taken us through how data should flow through a React/Redux app to keep it readable, maintainable, and predictable.
The [next post]({% post_url 2017-10-29-react-redux-discipline-2-enforcing-data-validity %}) in this series will explain how we can validate that the data flowing into our Redux store is the format that we expect, and why that's a good thing.

***

## Posts in this series
{% include series/toc-react-redux-discipline.md %}