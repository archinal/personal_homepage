---
layout: post
title:  "React/Redux Discipline: Part 2 - Enforcing data validity"
description: "Recommendations around keeping the data in your app valid at all times (or complaining loudly when it's not)."
img: "React_Redux_Discipline_Part_2.png"
date: 2017-10-29  +1100
categories: [javascript, react, redux]
---

In the [previous post in this series]({% post_url 2017-10-28-react-redux-discipline-1-state-management %}) I explained my recommendations for orchestrating the flow of data through a React/Redux app via the diagram below:

![React/Redux data flow]({{site.baseurl}}/assets/img/react_redux_data_flow.png)

In this post we will look at the blue "Data Validation" circles, using [Joi](https://github.com/hapijs/joi) as an example of a data validation tool, although many other good tools exist, like [`jsonschema`](https://www.npmjs.com/package/jsonschema).
(Note that if you're running Joi in the browser you will probably need to use [`joi-browser`](https://github.com/jeffbski/joi-browser) as well.)

## "Classes" or Plain Objects?
Before we proceed with discussion about data validation, you might like to make a decision regarding how you'll handle the data in your app.
In my experience, for applications where the same data types (like "Users", "Groups" and "Activities") are consistently used across the application, I've enjoyed formalising those objects as JavaScript classes, like the following:

{% highlight jsx %}

class User {
    constructor(data) {
        this.username = data.username;
        this.name = data.name;
        this.phone = data.phone;
        this.email = data.email;
    }

    getUsername() {
        return this.username;
    }

    getName() {
        return this.name && this.name.length > 0 ? this.name : this.getUsername();
    }

    getPhone() {
        return this.phone;
    }

    getEmail() {
        return this.email;
    }
}

export default User;

{% endhighlight %}

As a matter of personal preference, I like that I can use accessor methods on the object's data without necessarily needing to worry about what the attributes themselves are called.
It also helps formalize logic like "if the `User` doesn't have a name, present their username as their name", or to write more complex functions like `matchesQuery` for filtering.

If you'd prefer not to use classes, and to instead just use plain objects of the following format, that's fine. Again, the most important thing is that you're consistent.

{% highlight jsx %}
const user = {
    username: 'Alice',
    name: 'Alice Apple',
    phone: '100000',
    email: 'alice@example.com',
};
{% endhighlight %}

## When to validate
The data flow diagram above gives a good outline of when to validate the data. As a general rule of thumb, any time you're creating new data you should validate it.
One of the benefits of the class-based approach above is that you can perform the validation in the constructor, so if you have a `User` object it's guaranteed that its data has been validated (unless you then manually changed its attributes).
If you guarantee that all data that has entered the redux-store has been validated you'll have the added luxury of not needing to validate the low-level data on your React components 
(although you should still use [`prop-types`](https://reactjs.org/docs/typechecking-with-proptypes.html) to confirm that your components are receiving the data they expect).

## How to validate
If you're using [Joi](https://github.com/hapijs/joi) you will create a schema to match data against. 
If you're going with a class-based approach, this means you will probably have one schema for every class in your app.
Our schema for our `User` model will look like this:
{% highlight jsx %}

import Joi from 'joi-browser';

const userSchema = Joi.object().keys({
    username: Joi.string().regex(/^[a-z0-9-.]+$/).max(50).required(),
    name: Joi.string().max(255),
    phone: Joi.string().max(32),
    email: Joi.string().email().required(),
});

export default userSchema;

{% endhighlight %}

If you're using a class for `User` objects, you then amend the constructor to the following:
{% highlight jsx %}
import userSchema from './schemas/user';

class User {
    constructor(data) {
        const result = Joi.validate(data, userSchema);
        if (result.error !== null) {
            throw new Error(result.error);
        }
        sanitizedData = result.value;

        this.username = sanitizedData.username;
        this.name = sanitizedData.name;
        this.phone = sanitizedData.phone;
        this.email = sanitizedData.email;
    }
    
    // ...
}
{% endhighlight %}

If you're not using a class-based approach, it's a simple process:
{% highlight jsx %}
import userSchema from './schemas/user';

const data = {
   username: 'Alice',
   name: 'Alice Apple',
   phone: '100000',
   email: 'alice@example.com',
};

const result = Joi.validate(data, userSchema);
if (result.error !== null) {
    throw new Error(result.error);
}
const user = result.value;

{% endhighlight %}

There are two important things to note here:
1. We're basing our objects' data off the validation result, not their original input. 
This is because Joi lets us massage the data, provide defaults, strip unwanted keys, and much more, and we want to make full use of that functionality.
1. We're throwing (and not catching) an error if validation fails. 
I'm of the opinion that if something has gone wrong enough that you've received data you didn't expect (read: that you don't know how to handle) you need to fail quickly and loudly.
If you continue on, suppressing the error, you're only doing yourself a disservice when something downstream isn't working as expected and it takes you longer to diagnose the issue.
Before throwing the error it's a good idea to log the details with an error reporting tool like [Sentry](https://sentry.io/for/react/).

## Conclusion
In this post we covered when, and how to validate the data flowing through your application. 
In the [next post]({% post_url 2017-10-30-react-redux-discipline-2-5-form-validation %}) we'll run through an example of how to use these schemas with [`redux-form`](https://redux-form.com/) to provide real-time form validation to your users with minimal extra effort.

***

## Posts in this series
{% include series/toc-react-redux-discipline.md %}