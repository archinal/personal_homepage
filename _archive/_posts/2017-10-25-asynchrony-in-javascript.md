---
layout: post
title:  "Asynchrony in JavaScript"
description: "An analysis of callbacks, promises and async/await."
img: wire-phone.jpg
date: 2017-10-25  +1100
categories: [javascript]
---

## Background
One of JavaScript's most (unfairly) despised features is the pervasiveness of asynchrony in its ecosystem.

For many programming languages, like python, methods that deal with external systems, like HTTP requests and File I/O are synchronous, which means you can write code like the following and the interpreter won't execute the next line of code until a result has been obtained:

{% highlight python %}
# Python example of synchronous code
import requests

# Get a UUID from an external web service
resp = requests.get('https://httpbin.org/uuid')

# Print the response (the uuid)
print (resp.text)

'''
Outputs:
{
      "uuid": "d49ce406-ac29-49e7-af6a-9cbad902aefc"
}
'''

{% endhighlight %}

Conversely, JavaScript doesn't typically wait for these operations to be completed before executing the next line, rather the user provides a function ("callback") that is called when the operation has been completed.

The benefit of this approach is that, since JavaScript in the browser (specifically JavaScript that can access the user interface, or "DOM") is run on a single thread, the thread can continue performing tasks while it waits for the asynchronous operation to be completed.

The callback approach has several drawbacks, which I'll explain further below.
I'll also introduce two approaches that are designed to address the issues with callbacks - promises and async/await. For each of the approaches I will implement a simple asynchronous function that returns a random number, for illustration purposes.

## Examples
### Callbacks
{% highlight javascript %}
/*
As an example, this is a function which asynchronously generates a random integer between 1 and max inclusive.
If the result is an odd number, it will return an error
Otherwise it will return the result
*/
const asyncGetEvenNumberOrFail = (max, callback) => {
    setTimeout(() => {
        const randomNumber = Math.floor(Math.random() * max) + 1;
        if (randomNumber % 2 === 1) {
            const err = new Error('We got an odd number!');
            callback(err);
        } else {
            callback(null, randomNumber);
        }
    }, 100);
};

const callback = (err, result) => {
    if (err !== null) {
        console.log('Too bad, got an odd number');
    } else {
        console.log('Got even result:', result);
    }
};

const main = () => {
    asyncGetEvenNumberOrFail(5, callback);
}
{% endhighlight %}

### Promises
{% highlight javascript %}
const asyncGetEvenNumberOrFail = (max) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const randomNumber = Math.floor(Math.random() * max) + 1;
            if (randomNumber % 2 === 1) {
                const err = new Error('We got an odd number!');
                reject(err);
            } else {
                resolve(randomNumber);
            }
        }, 100);
    });
};

const main = () => {
    asyncGetEvenNumberOrFail(5)
        .then(result => {
            console.log('Got even result:', result);
        }).catch(err => {
            console.log('Too bad, got an odd number');
        });
};

main();
{% endhighlight %}

### Async/Await
{% highlight javascript %}
const asyncGetEvenNumberOrFail = (max) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const randomNumber = Math.floor(Math.random() * max) + 1;
            if (randomNumber % 2 === 1) {
                const err = new Error('We got an odd number!');
                reject(err);
            } else {
                resolve(randomNumber);
            }
        }, 100);
    });
};

// Note the keyword 'async' in the function definition here
const main = async () =>  {
    try {
        // Note the keyword 'await' here
        // It means the function returning a promise behaves like a synchronous function
        const result = await asyncGetEvenNumberOrFail(5);
        console.log('Got even result:', result);
    } catch (err) {
        // If a promise is rejected, an error is thrown, so we catch that here.
        console.log('Too bad, got an odd number');
    }
};

main();
{% endhighlight %}

## Analysis
### Callbacks
#### Pros
- Callbacks are deeply entrenched in convention, and the callback signature of `callback(err, result)` is used in numerous popular packages
- Since callbacks are just functions, they can be expected to work in older browsers without a polyfill

#### Cons
- If you call lots of asynchronous functions in sequence, you will enter "callback hell", where you code becomes unreadably nested.
{% highlight javascript %}
const fn1 = (callback) => {
    fn2((err2, result2) => {
        if (err2) {
            throw err2;
        }
        fn3((err3, result3) => {
            if (err3) {
                throw err3;
            }
            fn4((err4, result4) => {
                if (err4) {
                    throw err4;
                }
                // ... you get the idea
                callback(null, result4);
            });
        });
    });
};
{% endhighlight %}

### Promises
#### Pros
- Relative to callbacks, promises are really clean, and can help avoid "callback hell"
- The Node.js steering committee seems to prefer promises to callbacks, as Node.js 8 includes a utility function [promisify](https://nodejs.org/dist/latest-v8.x/docs/api/util.html#util_util_promisify_original)
that converts callback-oriented functions to promise-based functions. An example from the docs:
{% highlight javascript %}
const util = require('util');
const fs = require('fs');

const stat = util.promisify(fs.stat);
stat('.')
    .then((stats) => {
        // Do something with `stats`
    }).catch((error) => {
        // Handle the error.
    });
{% endhighlight %}
- The native `Promise` library provides useful utility functions like `Promise.all` which can be used to call multiple asynchronous functions, and resolves when they've all finished
- Error handling can be performed once, right at the end, with a `.catch` statement, rather than needing to check the value of `err` in all callbacks


#### Cons
- If you're calling several promise functions in sequence, where each relies on the previous one's result, you will still need to deeply nest.

{% highlight javascript %}
const asyncFn = () => {
    return promise1()
        .then(result1 => {
            return promise2(result1)
                .then(result2 => {
                    return promise3(result1, result2);
                });
        });
};
{% endhighlight %}
- Because the `Promise` library isn't supported in browsers like IE11, you may need to use a polyfill or a third party library like [q](https://www.npmjs.com/package/q) or [bluebird](https://www.npmjs.com/package/bluebird)
  - Third party implementations of promises are all subtly different, and it can be hard get them to play nicely together

### Async/Await
#### Pros
- Similarly to promises, the asynchronous code looks a lot cleaner, and can help avoid "callback hell"
- In cases where you call several async functions in sequence, each requiring the returned value of the one before, async/await still avoids nesting, whereas promises don't.

{% highlight javascript %}
const asyncFn = async () => {
    const result1 = await promise1();
    const result2 = await promise2(result1);
    return promise3(result1, result2);
};
{% endhighlight %}

- You can handle both synchronous and asynchronous errors in the same `catch` section of the `try/catch` block (although this can be a double edged sword, as it may invite you to make false assumptions about what caused the error to be thrown)


#### Cons
- Making asynchronous code look like synchronous code can be a double-edged sword. It may not be immediately obvious that a method call is asynchronous (whereas callbacks and `.then` calls are much more obvious).
- Async/Await isn't natively supported in Node.js 6 LTS (it is included in Node.js 8 though).

## Conclusion
Overall I would argue that callbacks shouldn't be used without good reason, such as if the code is for a public package that may need to be run on older browsers.

I think both promises and async/await are excellent, but I personally tend to use promises a lot more because:
- They're natively supported in Node.js 6 LTS
- Async functions are [only natively supported in ~72% of browsers](https://caniuse.com/#feat=async-functions) in use on the web today (October 2017).

Having said that, in cases where I can guarantee that it's supported (such as when I'm using babel), I find the conciseness of async/await code wins out.