#!/bin/bash

SITE_URL="will.archin.al"
AWS_PROFILE=$1
AWS_CLOUDFRONT_DISTRIBUTION_ID="EFFFK6DCCL0EH"

export JEKYLL_ENV=production

PROFILE_STR=""
if [[ $# -gt 0 ]]; then
    PROFILE_STR="--profile ${AWS_PROFILE}"
fi

cd `dirname $0`

rm -r _site/

bundle exec jekyll build --future

aws s3 sync _site/ s3://${SITE_URL}/ --delete ${PROFILE_STR}

aws cloudfront create-invalidation --distribution-id ${AWS_CLOUDFRONT_DISTRIBUTION_ID} --paths "/*" ${PROFILE_STR}
